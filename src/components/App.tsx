import * as React from "react";
import Header from "../partials/Header"
import { BrowserRouter as Router } from "react-router-dom";

export const App = () => (
  <div>
    <Router>
        <Header/>
    </Router>    
  </div>
);