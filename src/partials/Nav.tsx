import * as React from 'react';
import { NavLink } from 'react-router-dom';
import { MenuItemInterface } from "../types"

export interface NavInterface {
    navItems: MenuItemInterface[]
}

export const MenuItem = (({name, link}: MenuItemInterface)=>{
    return <li className='navItem'><NavLink to={`${link}`}>{name}</NavLink></li>
})

function Nav({navItems}: NavInterface) {

    let menuItems = navItems.map((props: MenuItemInterface)=>{
        return <MenuItem key={`navItem${props.name}`} {...props}/>
    })

    return (
        <nav>
            <ul className='navContainer'>
               {menuItems}
            </ul> 
        </nav>
    );
}

export default Nav;