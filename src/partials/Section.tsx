import * as React from 'react';

export interface SectionInterface{
    backgroundImage: string
    children?: React.ReactElement | React.ReactElement[]
    className? : string
    backgroundColor?: string
}
function Section({backgroundColor="", backgroundImage, children, className = ""}: SectionInterface) {
    return (
        <div className={`section ${className}`} style={{backgroundSize:"cover", backgroundImage: `url('${backgroundImage}')`}}>
            <div style={{background: backgroundColor}}>
                {children}
            </div>            
        </div>
    );
}

export default Section;