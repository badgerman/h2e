import React from 'react';
import { MenuItemInterface } from "../types"
import Button from "../partials/Button"

export interface BannerInterface {
    title: string,
    body: string
    cta?: MenuItemInterface[]
}
function Banner({title, body, cta} : BannerInterface) {

    let callsToAction;

    if(cta.length){
        callsToAction = cta.map(({name, link})=>{
            return <Button name={name} link={link}/>
        })
    }
    
    return (
        <div className='mainBanner'>
            <h1 className='bannerTitle'>{title}</h1>
            <p className='bannerBody' >{body}</p>
            {callsToAction}
        </div>
    );
}

export default Banner;