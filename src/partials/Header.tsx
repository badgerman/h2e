import * as React from "react";
import Section from "./Section"
import Logo from "./Logo"
import Nav from "./Nav"
import Banner from "./Banner"

//for mock purposes - this would be a fetch
import routes from "../../mockData/routes.json"

function Header() {

    return (
        <div>
           <Section
                backgroundColor= "rgba(3,30,50,.85)"
                backgroundImage="./assets/images/a.jpg"
                className="siteHeader"
           > 
                <div className="navBar">
                    <Logo/>
                    <Nav navItems={routes}/>
                </div>

                <Banner
                    title="Fulfilment, Distribution & Storage Solutions"
                    body="Based in our Cheshire (UK) 27,000 sq. ft. premises we offer 17,000 sq. ft. of premium secure warehouse space for bulk, white label, third party and retail fulfilment and distribution."
                    cta={[
                        {
                            name: "Our Service",
                            link: "/service"
                        },
                        {
                            name: "Join Our Team",
                            link: "/team"
                        }
                    ]}
                />

           </Section>
        </div>
    );
}

export default Header;