import React from 'react';
import { Link } from 'react-router-dom';
import {MenuItemInterface} from "../types"
function Button({name, link}: MenuItemInterface) {
    return (
        <div className='ctaButton'>
            <Link to={link}>{name}</Link>            
        </div>
    );
}

export default Button;