import * as React from 'react';
import { Link } from 'react-router-dom';

function Logo() {
    return (
        <Link to="/">
            <img src="./assets/images/logo.png"/>
        </Link>
    );
}

export default Logo;